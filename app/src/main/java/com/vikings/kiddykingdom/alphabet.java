package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class alphabet extends Activity implements OnClickListener
{
	TextView a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
	ImageView leftarrow, rightarrow;
	MediaPlayer tias;
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.alphabet);
		Typeface type = Typeface.createFromAsset(getAssets(),"fonts/comic.ttf"); 
		
		
		
		a = (TextView) findViewById (R.id.a);
		b = (TextView) findViewById (R.id.b);
		c = (TextView) findViewById (R.id.c);
		d = (TextView) findViewById (R.id.d);
		e = (TextView) findViewById (R.id.e);
		f = (TextView) findViewById (R.id.f);
		g = (TextView) findViewById (R.id.g);
		h = (TextView) findViewById (R.id.h);
		i = (TextView) findViewById (R.id.i);
		j = (TextView) findViewById (R.id.j);
		k = (TextView) findViewById (R.id.k);
		l = (TextView) findViewById (R.id.l);
		m = (TextView) findViewById (R.id.m);
		n = (TextView) findViewById (R.id.n);
		o = (TextView) findViewById (R.id.o);
		p = (TextView) findViewById (R.id.p);
		q = (TextView) findViewById (R.id.q);
		r = (TextView) findViewById (R.id.r);
		s = (TextView) findViewById (R.id.s);
		t = (TextView) findViewById (R.id.t);
		u = (TextView) findViewById (R.id.u);
		v = (TextView) findViewById (R.id.v);
		w = (TextView) findViewById (R.id.w);
		x = (TextView) findViewById (R.id.x);
		y = (TextView) findViewById (R.id.y);
		z = (TextView) findViewById (R.id.z);
		
		   a.setTypeface(type);
		   b.setTypeface(type);
		   c.setTypeface(type);
		   d.setTypeface(type);
		   e.setTypeface(type);
		   f.setTypeface(type);
		   g.setTypeface(type);
		   h.setTypeface(type);
		   i.setTypeface(type);
		   j.setTypeface(type);
		   k.setTypeface(type);
		   l.setTypeface(type);
		   m.setTypeface(type);
		   n.setTypeface(type);
		   o.setTypeface(type);
		   p.setTypeface(type);
		   q.setTypeface(type);
		   r.setTypeface(type);
		   s.setTypeface(type);
		   t.setTypeface(type);
		   u.setTypeface(type);
		   v.setTypeface(type);
		   w.setTypeface(type);
		   x.setTypeface(type);
		   y.setTypeface(type);
		   z.setTypeface(type);
		   leftarrow = (ImageView) findViewById (R.id.leftarrow);
		   leftarrow.setOnClickListener(this);
		   rightarrow = (ImageView) findViewById (R.id.rightarrow);
		   rightarrow.setOnClickListener(this);
		   a.setOnClickListener(this);
		   b.setOnClickListener(this);
		   c.setOnClickListener(this);
		   d.setOnClickListener(this);
		   e.setOnClickListener(this);
		   f.setOnClickListener(this);
		   g.setOnClickListener(this);
		   h.setOnClickListener(this);
		   i.setOnClickListener(this);
		   j.setOnClickListener(this);
		   k.setOnClickListener(this);
		   l.setOnClickListener(this);
		   m.setOnClickListener(this);
		   n.setOnClickListener(this);
		   o.setOnClickListener(this);
		   p.setOnClickListener(this);
		   q.setOnClickListener(this);
		   r.setOnClickListener(this);
		   s.setOnClickListener(this);
		   t.setOnClickListener(this);
		   u.setOnClickListener(this);
		   v.setOnClickListener(this);
		   w.setOnClickListener(this);
		   x.setOnClickListener(this);
		   y.setOnClickListener(this);
		   z.setOnClickListener(this);
		   
		   tias = (MediaPlayer) MediaPlayer.create(alphabet.this,R.raw.mary );
		   tias.start();
		   tias.setLooping(true);
		   
		   
	}
	
	public void onClick(View v) 
	{
		int id = v.getId();
		
		
		if ( id ==R.id.rightarrow)
			{

			tias.stop();
			Intent next = new Intent(alphabet.this, alpha1.class);
        startActivity(next);
			}

		else if ( id == R.id.leftarrow)
			{
			tias.stop();Intent previous = new Intent(alphabet.this, MainActivity.class);
        startActivity(previous);
			}
		else if ( id == R.id.a)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha1.class);
    startActivity(previous);
		}
		else if ( id == R.id.b)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha2.class);
    startActivity(previous);
		}
		else if ( id == R.id.c)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha3.class);
    startActivity(previous);
		}
		else if ( id == R.id.d)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha4.class);
    startActivity(previous);
		}
		else if ( id == R.id.e)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha5.class);
    startActivity(previous);
		}
		else if ( id == R.id.f)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha6.class);
    startActivity(previous);
		}
		else if ( id == R.id.g)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha7.class);
    startActivity(previous);
		}
		else if ( id == R.id.h)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha8.class);
    startActivity(previous);
		}
		else if ( id == R.id.i)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha9.class);
    startActivity(previous);
		}
		else if ( id == R.id.j)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha10.class);
    startActivity(previous);
		}
		else if ( id == R.id.k)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha11.class);
    startActivity(previous);
		}
		else if ( id == R.id.l)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha12.class);
    startActivity(previous);
		}
		else if ( id == R.id.m)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha13.class);
    startActivity(previous);
		}
		else if ( id == R.id.n)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha14.class);
    startActivity(previous);
		}
		else if ( id == R.id.o)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha15.class);
    startActivity(previous);
		}
		else if ( id == R.id.p)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha16.class);
    startActivity(previous);
		}
		else if ( id == R.id.q)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha17.class);
    startActivity(previous);
		}
		else if ( id == R.id.r)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha18.class);
    startActivity(previous);
		}
		else if ( id == R.id.s)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha19.class);
    startActivity(previous);
		}
		else if ( id == R.id.t)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha20.class);
    startActivity(previous);
		}
		else if ( id == R.id.u)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha21.class);
    startActivity(previous);
		}
		else if ( id == R.id.v)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha22.class);
    startActivity(previous);
		}
		else if ( id == R.id.w)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha23.class);
    startActivity(previous);
		}
		else if ( id == R.id.x)
		{
			tias.stop();Intent previous = new Intent(alphabet.this, alpha24.class);
    startActivity(previous);
		}
		else if ( id == R.id.y)
		{
			tias.stop();
			Intent previous = new Intent(alphabet.this, alpha25.class);
    startActivity(previous);
		}
		else if ( id == R.id.z)
		{

			tias.stop();
			Intent previous = new Intent(alphabet.this, alpha26.class);
    startActivity(previous);
		}
		
		
			
		
	}
	@Override 
	public void onBackPressed()
	{
		tias.stop();
		finish();
		Intent previous = new Intent(alphabet.this, MainActivity.class);

		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}
	@Override
	public void onPause()
	{
		super.onPause();
		
		tias.stop();
	
	}
	
}
