package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class rhymelist extends Activity implements OnClickListener
{
	TextView song1,song2,song3,song4,song5,song6;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.rhymelist);
		
		song1 = (TextView) findViewById (R.id.song1);
		song1.setOnClickListener(this);
		song2 = (TextView) findViewById (R.id.song2);
		song2.setOnClickListener(this);
		song3 = (TextView) findViewById (R.id.song3);
		song3.setOnClickListener(this);
		song4 = (TextView) findViewById (R.id.song4);
		song4.setOnClickListener(this);
		song5 = (TextView) findViewById (R.id.song5);
		song5.setOnClickListener(this);
		
	}
	@Override
	public void onBackPressed()
	{
		finish();
		Intent previous = new Intent(this, MainActivity.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
			
	}
	@Override
	public void onClick(View v) 
	{
		int id = v.getId();
		if (id== R.id.song1)
		{
			Intent rhyme = new Intent (rhymelist.this,rhyme1.class);
			startActivity(rhyme);
		}
		else if (id== R.id.song2)
		{
			Intent rhyme = new Intent (rhymelist.this,rhyme2.class);
			startActivity(rhyme);
		}
		else if (id== R.id.song3)
		{
			Intent rhyme = new Intent (rhymelist.this,rhyme3.class);
			startActivity(rhyme);
		}
		else if (id== R.id.song4)
		{
			Intent rhyme = new Intent (rhymelist.this,rhyme4.class);
			startActivity(rhyme);
		}
		else if (id== R.id.song5)
		{
			Intent rhyme = new Intent (rhymelist.this,rhyme5.class);
			startActivity(rhyme);
		}
		
	}

}
