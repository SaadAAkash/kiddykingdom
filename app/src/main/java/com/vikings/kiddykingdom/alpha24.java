package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.media.MediaPlayer;

public class alpha24 extends Activity
{
	MediaPlayer akash, akash1, akash2, akash3;
	
	private GestureDetectorCompat gestureDetectorCompat;

/***********************************************/
    protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.alpha24);

        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener());
        
        akash = (MediaPlayer) MediaPlayer.create( alpha24.this,R.raw.x1 );
        akash.start();
        
       
		 ImageView left =(ImageView) findViewById(R.id.leftarrow);
		ImageView right = (ImageView) findViewById (R.id.rightarrow);

		Typeface type = Typeface.createFromAsset(getAssets(),"fonts/comic.ttf"); 
		TextView ans1 = (TextView) findViewById (R.id.ans1);
		ans1.setTypeface(type);
		TextView ans2 = (TextView) findViewById (R.id.ans2);
		ans2.setTypeface(type);
		TextView ans3 = (TextView) findViewById (R.id.ans3);
		ans3.setTypeface(type);
		
		
		left.setOnClickListener(new OnClickListener()
		{            
	      public void onClick(View v) 
	      {
	    	  akash.stop();
	    	  startActivity(new Intent(getApplicationContext(), alpha23.class));               
	     }
		});
		
		right.setOnClickListener(new OnClickListener()
		{            
		      public void onClick(View v) 
		      {
		    	  
		    	  akash.stop();
		    	  startActivity(new Intent(getApplicationContext(), alpha25.class));               
		     }
			});
		
		
	        //akash.setLooping(true);
	       /* Delay (5);
	        
			akash1 = (MediaPlayer) MediaPlayer.create( alpha1.this,R.raw.apple );
			akash1.start();
	        Delay (5);
	       
			
			akash2 = (MediaPlayer) MediaPlayer.create( alpha1.this,R.raw.aeroplane );
			akash2.start();
	        Delay (5);
			//akash.setLooping(true);
			
			akash3 = (MediaPlayer) MediaPlayer.create( alpha1.this,R.raw.ant );
			akash3.start();
	        */
		
	}

/***********************************************/
    /*private void Delay(int Seconds) {
		// TODO Auto-generated method stub
    	long Time = 0;
        Time = System.currentTimeMillis();
        while( System.currentTimeMillis() <  Time+(Seconds*1000) );
	}
*/
/***********************************************/
	@Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
/***********************************************/
    class MyGestureListener extends GestureDetector.SimpleOnGestureListener 
    {
        //handle 'swipe left' action only

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) 
        {

         /*
         Toast.makeText(getBaseContext(),
          event1.toString() + "\n\n" +event2.toString(),
          Toast.LENGTH_SHORT).show();
         */

            if(event2.getX() < event1.getX()){
            	akash.stop();
            	Intent intent = new Intent(
                        alpha24.this, alpha23.class);
            	akash.stop();
                startActivity(intent);
                akash.stop();
                
            }
            //going to prev 
            else if(event2.getX() > event1.getX()){
            	akash.stop();
            	Intent intent = new Intent(
                        alpha24.this, alpha25.class);
            	akash.stop();
                startActivity(intent);
                akash.stop();
            }
            akash.stop();
            return true;
        }
    }

/***********************************************/
	@Override
	public void onBackPressed()
	{
		
		akash.stop();
		/*akash1.stop();
		akash2.stop();
		akash3.stop();
		*/
		finish();
		
		Intent previous = new Intent(alpha24.this, alphabet.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}
}