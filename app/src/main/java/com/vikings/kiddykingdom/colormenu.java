package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class colormenu extends Activity implements OnClickListener 
{

	AnimationDrawable frameAnimation;
	ImageView view;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.colormenu);
		view = (ImageView) findViewById(R.id.imageAnimation);
		view.setBackgroundResource(R.drawable.coloranimation);

		// Typecasting the Animation Drawable
		frameAnimation = (AnimationDrawable) view.getBackground();
		
		frameAnimation.start();
		TextView caps = (TextView) findViewById (R.id.caps);
		/*TextView small = (TextView) findViewById (R.id.small);*/
		TextView ans1 = (TextView) findViewById (R.id.ans1);
		TextView ans2 = (TextView) findViewById (R.id.ans2);
		TextView ans3 = (TextView) findViewById (R.id.ans3);
		TextView ans4 = (TextView) findViewById (R.id.ans4);
		TextView ans5 = (TextView) findViewById (R.id.ans5);
		TextView ans6 = (TextView) findViewById (R.id.ans6);
		TextView ans7 = (TextView) findViewById (R.id.ans7);
		TextView ans8 = (TextView) findViewById (R.id.ans8);
		TextView ans9 = (TextView) findViewById (R.id.ans9);
		TextView ans10 = (TextView) findViewById (R.id.ans10);
		
		Animation anim_in= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.anim_in);
		Animation anim_out = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.anim_out);
		Animation bounce =   AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);

		ans1.startAnimation(bounce);
		ans2.startAnimation(bounce);
		ans3.startAnimation(anim_out);
		ans4.startAnimation(anim_out);
		ans5.startAnimation(anim_out);
		ans6.startAnimation(anim_out);
		ans7.startAnimation(anim_out);
		ans8.startAnimation(anim_out);
		ans9.startAnimation(anim_out);
		ans10.startAnimation(anim_out);
	}
	
	@Override
	public void onClick(View v) 
	{
		
	}
	
	@Override 
	public void onBackPressed()
	{
		finish();
		Intent previous = new Intent(colormenu.this, MainActivity.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}
	
}
