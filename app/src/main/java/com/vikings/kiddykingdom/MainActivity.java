package com.vikings.kiddykingdom;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements OnClickListener
{
	ImageView view,color,abc,rhyme,number;
	AnimationDrawable frameAnimation;
	private boolean doubleBackToExitPressedOnce;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.activity_main);
		// Typecasting the Image View
		view = (ImageView) findViewById(R.id.imageAnimation);
		view.setBackgroundResource(R.drawable.animation_list);
		// Typecasting the Animation Drawable
		frameAnimation = (AnimationDrawable) view.getBackground();
		frameAnimation.start();
		Animation slideleft = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slidingformenu2);
		Animation slideright = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slidingformenu);
		 abc = (ImageView) findViewById (R.id.abc);
		abc.startAnimation(slideleft);
		color = (ImageView) findViewById (R.id.color);
		color.startAnimation(slideleft);
		number = (ImageView) findViewById (R.id.number);
		//number.startAnimation(slideright);
		rhyme = (ImageView) findViewById (R.id.rhyme);
		//rhyme.startAnimation(slideright);
		

		abc.setOnClickListener(this);
		number.setOnClickListener(this);
		rhyme.setOnClickListener(this);
		color.setOnClickListener(this);
		
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{
		int id = v.getId();
		if (id== R.id.abc)
		{
			
			Intent ob = new Intent(MainActivity.this, alphabet.class);
			finish();
	        startActivity(ob);
		}
		else if (id== R.id.number)
		{
			
			Intent ob = new Intent(MainActivity.this, numerical.class);
	        startActivity(ob);
		}
		else if (id== R.id.rhyme)
		{
			
			Intent ob = new Intent(MainActivity.this, rhymelist.class);
	        startActivity(ob);
		}
		else if (id== R.id.color)
		{
			
			Intent ob = new Intent(MainActivity.this, colormenu.class);
	        startActivity(ob);
		}
		
		
			
		
	}
	
	@Override
	public void onBackPressed()
	{
			finish();
			Intent intent = new Intent (Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			
			
	}
}
