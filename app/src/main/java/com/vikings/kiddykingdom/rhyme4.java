package com.vikings.kiddykingdom;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class rhyme4 extends Activity implements OnClickListener 
{
	AnimationDrawable frameAnimation;
	ImageView blacksheep;
	MediaPlayer tias;
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
		setContentView(R.layout.rhyme4);
		blacksheep = (ImageView) findViewById(R.id.blacksheep);
		blacksheep.setBackgroundResource(R.drawable.mary);
		frameAnimation = (AnimationDrawable) blacksheep.getBackground();
		frameAnimation.start();
		tias = (MediaPlayer) MediaPlayer.create(rhyme4.this,R.raw.mary );
		tias.start();
		tias.setLooping(true);
		
		
		
	}

	@Override
	public void onClick(View arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override 
	public void onBackPressed()
	{
		tias.stop();
		finish();
		Intent previous = new Intent(rhyme4.this, rhymelist.class);
		previous.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(previous);
	
	}

}
